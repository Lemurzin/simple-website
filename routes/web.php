<?php

use Illuminate\Support\Facades\Route;

require __DIR__.'/auth.php';

//Auth
Route::get('/dashboard', function () {
    return redirect()->route('admin.index');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';

Route::group(['namespace'=>'admin','prefix'=>'admin','middleware'=>['auth','admin']],function(){
    Route::get('/','IndexController')->name('admin.index');
    Route::patch('/{id}','ClientConfController@update')->name('admin.conf.update');
    Route::post('/','ClientConfController@store')->name('admin.conf.store');

    Route::group(['prefix'=>'categories'],function(){
        Route::get('/','CategoryController@index')->name('admin.category.index');
        Route::get('/create','CategoryController@create')->name('admin.category.create');
        Route::post('/','CategoryController@store')->name('admin.category.store');
        Route::get('/edit/{id}','CategoryController@edit')->name('admin.category.edit');
        Route::patch('/{id}','CategoryController@update')->name('admin.category.update');
        Route::delete('{id}','CategoryController@destroy')->name('admin.category.destroy');
    });

    Route::group(['namespace'=>'page', 'prefix'=>'pages'],function(){
        Route::get('/','PageController@index')->name('admin.page.index');
        Route::get('/create','PageController@create')->name('admin.page.create');
        Route::post('/','PageController@store')->name('admin.page.store');
        Route::get('/edit/{id}','PageController@edit')->name('admin.page.edit');
        Route::patch('/{id}','PageController@update')->name('admin.page.update');
        Route::delete('{id}','PageController@destroy')->name('admin.page.destroy');
    });

    Route::group(['prefix'=>'banners'],function(){
        Route::get('/','BannerController@index')->name('admin.banner.index');
        Route::get('/create','BannerController@create')->name('admin.banner.create');
        Route::post('/','BannerController@store')->name('admin.banner.store');
        Route::get('/edit/{banner}','BannerController@edit')->name('admin.banner.edit');
        Route::patch('/{banner}','BannerController@update')->name('admin.banner.update');
        Route::delete('{banner}','BannerController@destroy')->name('admin.banner.destroy');
    });

    Route::group(['prefix'=>'carousel'],function(){
        Route::get('/','CarouselController@index')->name('admin.carousel.index');
        Route::get('/create','CarouselController@create')->name('admin.carousel.create');
        Route::post('/','CarouselController@store')->name('admin.carousel.store');
        Route::get('/edit/{carousel}','CarouselController@edit')->name('admin.carousel.edit');
        Route::patch('/{carousel}','CarouselController@update')->name('admin.carousel.update');
        Route::get('/{carousel}','CarouselController@show')->name('admin.carousel.show');
        Route::delete('{carousel}','CarouselController@destroy')->name('admin.carousel.destroy');
    });

    Route::group(['prefix'=>'blocks'],function(){
        Route::get('/','BlockController@index')->name('admin.block.index');
        Route::get('/create','BlockController@create')->name('admin.block.create');
        Route::post('/','BlockController@store')->name('admin.block.store');
        Route::get('/edit/{block}','BlockController@edit')->name('admin.block.edit');
        Route::patch('/{block}','BlockController@update')->name('admin.block.update');
        Route::delete('{block}','BlockController@destroy')->name('admin.block.destroy');
    });

    Route::group(['prefix'=>'image'],function(){
            Route::get('/','ImageController@index')->name('admin.image.index');
            Route::get('/{path}/create','ImageController@create')->name('admin.image.create');
            Route::post('/','ImageController@store')->name('admin.image.store');
            Route::get('/{name}','ImageController@show')->name('admin.image.show');
            Route::delete('/{path}','ImageController@destroy')->name('admin.image.destroy');
    }); 

    Route::group(['prefix'=>'users'], function(){
        Route::get('/','UserController@index')->name('admin.user.index');
        Route::get('/create','UserController@create')->name('admin.user.create');
        Route::post('/','UserController@store')->name('admin.user.store');
        Route::get('/{user}/edit','UserController@edit')->name('admin.user.edit');
        Route::patch('/{user}','UserController@update')->name('admin.user.update');
        Route::delete('/{user}','UserController@destroy')->name('admin.user.destroy');
    });

    Route::group(['prefix'=>'directory'],function(){
        Route::post('/','DirectoryController@store')->name('admin.directory.store');
        Route::delete('/{name}','DirectoryController@destroy')->name('admin.directory.destroy');
    });
});

Route::group(['namespace'=>'client'],function(){
    Route::get('/','IndexController')->name('index');
    Route::get('/{id}','CategoryController')->name('category.index');
    Route::get('/pages/{id}','PageController@show')->name('page.show');
});

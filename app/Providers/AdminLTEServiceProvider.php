<?php

namespace App\Providers;

use App\Models\Page;
use App\Models\Category;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AdminLTEServiceProvider extends ServiceProvider
{

    public function register()
    {
        //
    }

    public function boot()
    {
        View::composer('admin.layouts.nav', function ($view) {
            $counts = [
                'category'=> Category::all()->count(),
                'page'=> Page::all()->count(),
            ];
            $view->with('counts',$counts);
        });
    }
}

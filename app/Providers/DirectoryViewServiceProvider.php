<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class DirectoryViewServiceProvider extends ServiceProvider
{
    //protected $activeDirectory;

    public function register()
    {
        //
    }

    public function boot()
    {
        View::composer('admin.image.*', function ($view) {
            $paths = Storage::directories('public/images/posts');
            $storages = [];
            foreach ($paths as $path) {
                $item = $path;
                while ($pos = strpos($item,'/')) {
                    $item = substr($item, ++$pos);
                }
                $storages[] = ['path' => $path,'name' => $item];
            }

            $view->with('storages',$storages);
        });
    }
}

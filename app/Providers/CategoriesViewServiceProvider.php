<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class CategoriesViewServiceProvider extends ServiceProvider
{
    //protected $activeDirectory;

    public function register()
    {
        //
    }

    public function boot()
    {
        View::composer('client.layouts.nav', function ($view) {
            $categories = Category::all();
            $view->with('categories', $categories);
        });
    }
}

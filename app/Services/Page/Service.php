<?php

namespace App\Services\Page;

use App\Models\Page;
use Illuminate\Support\Facades\Storage;

include '../vendor/simplehtmldom/simplehtmldom/simple_html_dom.php';

define("FILE_SAVE_PATH", "images/posts/");

class Service{
    public function store($data){

        $data = $this->parse($data);

        $data = $this->saveFile($data);

        Page::create($data);

    }

    public function update($data, $id)
    {
        $data = $this->parse($data);

        $data = $this->saveFile($data);

        $page = Page::find($id);
        $page->update($data);

        return $page;
    }

    private function parse($data)
    {
        $content = str_get_html($data['content']);
        if($content->find('img',0))
        {
            $types =[
                'png'=>'data:image/png;base64,',
                'jpg'=>'data:image/jpg;base64,',
                'jpeg'=>'data:image/jpeg;base64,',
            ];

            $images =[];
            foreach($content->find('img') as $img)
            {
                foreach($types as $type)
                {
                    if(str_contains($img->src,$type))
                    {
                        $images[] = [
                            'name' => $img->attr['data-filename'],
                            'style'=> $img->style,
                            'url' => request()->root().'/storage/'.FILE_SAVE_PATH.date('dMY').'/'.$img->attr['data-filename'],
                            'base64' => $img->src,
                            'base64_n' => str_replace($type,'',$img->src),
                        ];
                        break;
                    }
                }
            }
            if(isset($images))
            {
                foreach($images as $img)
                {
                    Storage::disk('public')->put(FILE_SAVE_PATH.date('dMY').'/'.$img['name'],base64_decode($img['base64_n']));
                    $data['content'] = str_replace($img['base64'],$img['url'],$data['content']);
                }
            }
        }
        return $data;
    }

    private function saveFile($data)
    {
        if(isset($data['image'])){
            $main_image = $data['image'];
            $path_image = Storage::disk('public')->put(FILE_SAVE_PATH.date('dMY'),$main_image);
            $data['image'] = $path_image;
        }
        return $data;
    }
}
<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


class AdminPanelMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if(isset(auth()->user()->role)){                                      
            if(auth()->user()->role === 'admin'){
                return $next($request);
            }
        }
        elseif(Route::is('admin.main.index')){
            return redirect('/');
        }
        else{
            return abort(404);
        }
    }
    
}
<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:60',
            'content' => 'required|string',
            'subtitle' => 'nullable|string|max:60',
            'image' => 'required|file|mimes:png,jpg,jpeg',
            'active' => 'min:0|max:2',
            'category_id' => 'required|string|exists:categories,id',
        ];
    }
}

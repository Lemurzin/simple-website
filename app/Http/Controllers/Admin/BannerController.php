<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Models\Banner;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    public function index()
    {
        $banners = Banner::all();
        return view('admin.banner.index',compact('banners'));
    }

    public function create()
    {
        $pages = Page::all();
        if($pages->first())
        {
            return view('admin.banner.create',compact('pages'));
        }
        else
        {
            return redirect()->route('admin.page.create');
        }
    }

    public function store(Request $request)
    {
        $data = $request->validate(
            [
                'title' => 'required|string|min:3|max:50',
                'page_id' => 'required|integer|exists:pages,id'
            ]
        );
        Banner::create($data);
        return redirect()->route('admin.banner.index');
    }

    public function edit(Banner $banner)
    {
        $pages = Page::all();
        return view('admin.banner.edit',compact('banner','pages'));
    }

    public function update(Request $request, Banner $banner)
    {
        $data = $request->validate(
            [
                'title' => 'required|string|min:3|max50',
                'page_id' => 'required|integer|exists:pages,id',
            ]
        );
        $banner->update($data);
        return redirect()->route('admin.banner.show');
    }

    public function destroy(Banner $banner)
    {
        $banner->delete();
        return redirect()->route('admin.banner.index');
    }
}

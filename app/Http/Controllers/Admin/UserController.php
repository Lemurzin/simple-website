<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.user.index',compact('users'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users|email',
            'role' => 'required|string',
            'password' => 'nullable|string|min:8|max:30',
        ]);
        $data['password'] = Hash::make($data['password']);
        User::firstOrCreate($data);
        return redirect()->route('admin.user.index');
    }

    public function edit(string $id)
    {
        $user = User::find($id);
        return view('admin.user.edit',compact('user'));
    }

    public function update(Request $request, string $id)
    {
        $user = User::find($id);
        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users,email,'.$user->id,
            'role' => 'required|string',
            'password' => 'required|string|min:8|max:30',
        ]);
        $data['password'] = Hash::make($data['password']);
        $user->update($data);

        if(auth()->user()->id == $id){
            return redirect()->route('logout');
        }
        else{
            return redirect()->route('admin.user.index');
        }
       
    }

    public function destroy(string $id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('admin.user.index');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\CarouselImage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CarouselController extends Controller
{
    public function index()
    {
        $carousel = CarouselImage::all();
        return view('admin.carousel.index',compact('carousel'));
    }

    public function create()
    {
        return view('admin.carousel.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate(
            [
                'title' => 'required|string|min:5|max:300',
                'image' => 'required|file|mimes:png,jpg,jpeg',
                'subtitle' => 'required|string|min:5|max:300'
            ]
        );

        if(isset($data['image'])){
            $main_image = $data['image'];
            $path_image = Storage::disk('public')->put('images/posts/banners/',$main_image);
            $data['image'] = $path_image;
        }

        CarouselImage::create($data);
        return redirect()->route('admin.carousel.index');
    }

    public function show(CarouselImage $carousel)
    {
        return view('admin.carousel.show',compact('carousel'));
    }

    public function edit(CarouselImage $carousel)
    {
        return view('admin.carousel.edit',compact('carousel'));
    }

    public function update(Request $request, CarouselImage $carousel)
    {
        $data = $request->validate(
            [
                'title' => 'required|string|min:5|max:300',
                'image' => 'nullable|file|mimes:png,jpg,jpeg',
                'subtitle' => 'required|string|min:5|max:300'
            ]
        );
        
        if(isset($data['image'])){
            $main_image = $data['image'];
            $path_image = Storage::disk('public')->put('images/posts/banners/',$main_image);
            $data['image'] = $path_image;
        }

        $carousel->update($data);
        return redirect()->route('admin.carousel.show', $carousel);
    }

    public function destroy(CarouselImage $carousel)
    {
        $carousel->delete();
        return redirect()->route('admin.carousel.index');
    }
}
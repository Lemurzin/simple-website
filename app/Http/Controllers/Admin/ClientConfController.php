<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ConfMain;
use Illuminate\Http\Request;

class ClientConfController extends Controller
{
   
    public function update(Request $request, string $id)
    {
        $conf = ConfMain::find($id);
        $data = $request->validate(
            [
                'hasCarousel' => 'min:0|max:2',
                'hasBanner' => 'min:0|max:2',
                'hasLatest' => 'min:0|max:2',
            ]
        );

        //or required
        if(!isset($data['hasCarousel']))
            $data['hasCarousel'] = false;
        if(!isset($data['hasBanner']))
            $data['hasBanner'] = false;
        if(!isset($data['hasLatest']))
            $data['hasLatest'] = false;
            
        $conf->update($data);
        return redirect()->route('admin.index');
    }

    public function store(Request $request)
    {
        $data = $request->validate(['hidden' => 'required']);
        if($data['hidden'] === 'exists')
        {
            ConfMain::create(['title'=>'first']);
        }
       
        return redirect()->route('admin.index');
    }
}

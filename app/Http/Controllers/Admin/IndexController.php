<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Models\ConfMain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function __invoke(){ 
        $conf = ConfMain::first();    
        $data = [
            [
                'title' => 'configurable value',
                'value' => random_int(0,200),
            ],
            [
                'title' => 'сount pages',
                'value' => Page::all()->count(),
            ],
            [
                'title' => 'configurable value',
                'value' => random_int(0,200),
            ],
            [
                'title' => 'configurable value',
                'value' => random_int(0,200),
            ]
        ];
        return view('admin.main.index',compact('data','conf'));
   }
}

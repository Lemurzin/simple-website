<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

define("FILES_PATH", "images/posts/");
class ImageController extends Controller
{
    
    public function index()
    {
        return view('admin.image.index');
    }
    
    public function create($path)
    {
        return view('admin.image.create',compact('path'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'image' => 'file|mimes:png,jpg,jpeg',
            'directory' => 'string',
            'filename' =>'string|max:20',
        ]);
        Storage::disk('public')->put(FILES_PATH.$data['directory'],$data['image']);
        return view('admin.image.index');
    }

    public function show($name)
    {    
        $path_files = 'public/'.FILES_PATH.$name;
        $files = Storage::allFiles($path_files);
        $images =[];
        foreach($files as $file)
        {
            $images[] = [
                'name' => str_replace($path_files.'/','',$file),
                'path' => substr($file, strpos($file,'/')+1),
            ];
        }
        return view('admin.image.show',compact('name','images'));
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Models\Block;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlockController extends Controller
{
    public function index()
    {
        $blocks = Block::all();
        return view('admin.block.index',compact('blocks'));
    }

    public function create()
    {
        $pages = Page::all();
        if($pages->first())
        {
            return view('admin.block.create',compact('pages'));
        }
        else
        {
            return redirect()->route('admin.page.create');
        }
    }

    public function store(Request $request)
    {
        $data = $request->validate(
            [
                'title' => 'required|string|max:150',
                'position' => 'required|integer|min:0|max:100',
                'isActive' => 'min:0|max:2',
                'pages' => 'required|array',
                'pages.*' => 'required|integer|exists:pages,id',
            ]
        );
       
        $json = json_encode($data['pages']);
        unset($data['pages']);

        $data['page_ids'] = $json;

        Block::create($data);
        return redirect()->route('admin.block.index');
    }

    public function edit(Block $block)
    {
        $pages = Page::all();
        /*
        foreach(json_decode($block->page_ids) as $el)
        {
            $blocks[$key]['pages'][] = Page::find($el);
        }*/
        return view('admin.block.edit',compact('block','pages'));
    }

    public function update(Request $request, Block $block)
    {
        $data = $request->validate(
            [
                'title' => 'required|string|max:150',
                'position' => 'required|integer|min:0|max:100',
                'isActive' => 'min:0|max:2',
                'pages' => 'required|array',
                'pages.*' => 'required|integer|exists:pages,id',
            ]
        );
        
        $json = json_encode($data['pages']);
        unset($data['pages']);

        $data['page_ids'] = $json;

        $block->update($data);
        return redirect()->route('admin.block.index');
    }

    public function destroy(Block $block)
    {
        $block->delete();
        return redirect()->route('admin.block.index');
    }
}
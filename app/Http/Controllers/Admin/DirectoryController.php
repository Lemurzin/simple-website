<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DirectoryController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:30',
        ]);
        
        $directories = Storage::directories('public/images/posts');

        $isExist = false;

        foreach($directories as $el)
        {
            if($el == 'public/images/posts/'.$data['name'])
            {
                $isExist = true;
                break;
            }
        }
        if( !$isExist ){Storage::makeDirectory('public/images/posts/'.$data['name']);}
        
        return redirect()->route('admin.image.index');
    }

    public function destroy($name)
    {
        Storage::deleteDirectory('public/images/posts/'.$name);
        return redirect()->route('admin.image.index');

    }
}

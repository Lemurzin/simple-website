<?php

namespace App\Http\Controllers\Admin\Page;

use App\Models\Page;
use App\Models\Category;
use App\Http\Filters\PageFilter;
use App\Http\Requests\Page\StoreRequest;
use App\Http\Requests\Page\FilterRequest;
use App\Http\Requests\Page\UpdateRequest;
use App\Http\Controllers\Admin\Page\BaseController;

class PageController extends BaseController
{
    public function index(FilterRequest $request)
    {
        $data = $request->validated();
        $filter = app()->make(PageFilter::class,['queryParams' => array_filter($data)]);
        $pages=Page::filter($filter)->orderBy('id', 'desc')->paginate(10);
        return view('admin.page.index',compact('pages'));
    }


    public function create()
    {
        $categories = Category::all();
        if($categories->first())
        {
            return view('admin.page.create',compact('categories'));
        }
        else{
            return redirect()->route('admin.category.create');
        }
    }


    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        $this->service->store($data);

        return redirect()->route('admin.page.index');
    }

    public function edit($id)
    {
        $page = Page::find($id);
        $categories = Category::all();
        return view('admin.page.edit',compact('page'),compact('categories'));
    }


    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();

        $page = $this->service->update($data, $id);

        return redirect()->route('admin.page.index');
    }


    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();
        return redirect()->route('admin.page.index');
    }
}

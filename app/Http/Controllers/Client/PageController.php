<?php

namespace App\Http\Controllers\Client;

use App\Models\Page;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function show($id)
    {
        $page = Page::find($id);
        return view('client.pages.show',compact('page'));
    }
}

<?php

namespace App\Http\Controllers\Client;

use App\Models\Page;
use App\Models\Block;
use App\Models\Banner;
use App\Models\ConfMain;
use App\Models\CarouselImage;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function __invoke()
    {
        $conf = ConfMain::first();
        $carousel = CarouselImage::all();
        $banner = Banner::orderBy('created_at','DESC')->take(4)->get();
        $latests = Page::orderBy('created_at','DESC')->take(4)->get();

        $b_active = Block::orderBy('position','ASC')->where('isActive', true)->get();
        $blocks = [];
        foreach($b_active as $key=>$block)
        {
            $blocks[$key]['title'] = $block->title;
            foreach(json_decode($block->page_ids) as $el)
            {
                $blocks[$key]['pages'][] = Page::find($el);
            }
        }

        return view('client.main.index',compact('conf', 'carousel', 'banner', 'latests', 'blocks'));
    }
}

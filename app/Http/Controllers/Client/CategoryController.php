<?php

namespace App\Http\Controllers\Client;

use App\Models\Category;
use App\Http\Controllers\Controller;


class CategoryController extends Controller
{
    public function __invoke($id){
        if($category = Category::find($id))
        {
            $pages = $category->pages()->where('active','1')->orderBy('created_at','DESC')->paginate(17);
            return view('client.pages.index',compact('pages','category'));
        }
        else{
            abort(404);
        }       
    }
}

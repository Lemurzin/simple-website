<?php

namespace App\Models;

use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;
    use Filterable;
    protected $table = 'categories';
    protected $guarded = false;
    public $incrementing = false;
    

    public function pages(){
        return $this->hasMany(Page::class,'category_id','id');
    }
}

<?php

namespace App\Models;

use App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Banner extends Model
{
    use HasFactory;
    protected $table = 'banners';
    protected $guarded = false;

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}

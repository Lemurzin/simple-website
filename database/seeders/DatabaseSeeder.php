<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\Block;
use App\Models\Banner;
use App\Models\Category;
use App\Models\ConfMain;
use App\Models\CarouselImage;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Category::factory(4)->create();
        Page::factory(250)->create();
        CarouselImage::factory(5)->create();
        ConfMain::factory(1)->create();
        Banner::factory(4)->create();
        Block::factory(4)->create();
        
        \App\Models\User::create(
            ['name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'role' => 'admin']
        );
        
        // \App\Models\User::factory(10)->create();
    }
}

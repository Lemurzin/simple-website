<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class PageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'content' => $this->faker->text(),
            'subtitle'=> $this->faker->word(),
            'image' => 'seed/img/'.random_int(1,6).'.jpg',
            'active' => '1',
            'category_id' => Category::get()->random()->id,

        ];
    }
}

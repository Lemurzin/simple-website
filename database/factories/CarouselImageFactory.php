<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CarouselImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
       
        return [
            'title' => $this->faker->text(),
            'subtitle'=> $this->faker->text(),
            'image' => 'seed/banner/banner'.random_int(1,2).'.jpg',
        ];
    }
}

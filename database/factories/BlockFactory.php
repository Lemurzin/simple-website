<?php

namespace Database\Factories;

use App\Models\Page;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlockFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $r = random_int(4,6);

        $array = [];
        for ($i=0; $i < $r; $i++) { 
            $array[$i] = Page::get()->random()->id;
        }
       
        $json = json_encode($array);
        
        return [
            'title' => $this->faker->word(),
            'position' => random_int(0,100),
            'isActive' => random_int(0,1),
            'page_ids' => $json,
        ];
    }
}

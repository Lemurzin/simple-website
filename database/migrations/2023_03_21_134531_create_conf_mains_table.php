<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfMainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conf_mains', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->boolean('hasLatest')->default(true);
            $table->boolean('hasCarousel')->default(true);
            $table->boolean('hasBanner')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conf_mains');
    }
}

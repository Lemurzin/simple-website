Installation

Clone the repository
Switch to the repo folder
Install all the dependencies using composer
```
composer install
```
Copy the example env file and make the required configuration changes in the .env file
```
cp .env.example .env
```
Generate a new application key
```
php artisan key:generate
```
Сreate a symbolic link from public/storage to storage/app/public
```
php artisan storage:link
```
Run the database migrations (Set the database connection in .env before migrating). The --seed command populates the database with test data.
```
php artisan migrate:fresh --seed
```
Start the local development server
```
php artisan serve
```
Admin panel login information
```
admin@gmail.com
admin
```
@extends('client.layouts.index')
@section('content')


  @isset($conf)
    @if ($conf->hasBanner && isset($banner))
      <!--banner-->
      <div class="banner-top-thumb-wrap">
        <div class="d-lg-flex justify-content-between align-items-center">
          @foreach ($banner as $item)
          <div class="d-flex justify-content-between  mb-3 mb-lg-0">
            <div>
              <img
                src="{{asset('storage/'.$item->page->image)}}"
                class="banner-top-thumb"
              />
            </div>
            <a style="text-decoration: none; color: inherit;" href="{{route('page.show',$item->page_id)}}">
              <h5 class="m-0 font-weight-bold">
                {{$item->page->title}}
              </h5>
            </a>
          </div>
          @endforeach
        </div>
      </div>
    @endif

    @if ($conf->hasCarousel && isset($carousel))
      <!--carousel-->
      <div class="row">
        <div class="col-lg-12">
          <div class="owl-carousel owl-theme" id="main-banner-carousel">
            @foreach ($carousel as $item)
            <div class="item">
              <div class="carousel-content-wrapper mb-2">
                <div class="carousel-content">
                  <h1 class="font-weight-bold">
                    {{$item->title}}
                  </h1>
                  <h5 class="font-weight-normal  m-0">
                    {{$item->subtitle}}
                  </h5>
                </div>
                <div class="carousel-image">
                  <img src="{{asset('storage/'.$item->image)}}"/>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    @endif

    @if ($conf->hasLatest && isset($latests))
      <!--latest-news-->
      <div class="world-news">
        <div class="row">
          <div class="col-sm-12">
            <div class="d-flex position-relative  float-left">
              <h3 class="section-title">Latest News</h3>
            </div>
          </div>
        </div>
        <div class="row">
          @foreach ($latests as $key=>$item)
          <div class="col-lg-3 col-sm-6 {{$key < 1 ? "grid-margin" : ""}} mb-5 mb-sm-2">
            <div class="position-relative image-hover">
              <a style="text-decoration:none" href="{{route('page.show',$item->id)}}">
                <img
                  src="{{asset('storage/'.$item->image)}}"
                  class="img-fluid"
                  alt="world-news"
                />
                <span class="thumb-title">{{$item->category_id}}</span>
              </a>
            </div>
            <h5 class="font-weight-bold mt-3">
              {{$item->title}}
            </h5>
            <p class="fs-15 font-weight-normal">
              {{$item->subtitle}}
            </p>
          </div>
          @endforeach
        </div>
      </div>
    @endif
  @endisset


  @foreach ($blocks as $block)
    @if (count($block['pages']) <= 4)
      @include('client.block.4',$block)
    @endif
    @if (count($block['pages']) === 5)
      @include('client.block.5',$block)
    @endif
    @if (count($block['pages']) >= 6)
      @include('client.block.6',$block)
    @endif
  @endforeach

  
  
@endsection
   
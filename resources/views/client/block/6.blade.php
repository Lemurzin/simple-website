  <!--popular-news-->
  <div class="popular-news">
    <div class="row">
      <div class="col-lg-3">
        <div class="d-flex position-relative float-left">
          <h3 class="section-title">{{$block['title']}}</h3>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="row">
          @foreach ($block['pages'] as $key=>$item)
          <div class="col-sm-4  mb-5 mb-sm-2">
            <div class="position-relative image-hover">
              <a style="text-decoration:none" href="{{route('page.show',$item->id)}}">
                <img
                  src="{{asset('storage/'.$item->image)}}"
                  class="img-fluid"
                  alt="world-news"
                />
                <span class="thumb-title">{{$item->category_id}}</span>
              </a>
            </div>
            <h5 class="font-weight-600 mt-3">
              {{$item->title}}
            </h5>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
<div class="world-news">
    <div class="row">
      <div class="col-sm-12">
        <div class="d-flex position-relative  float-left">
          <h3 class="section-title">{{$block['title']}}</h3>
        </div>
      </div>
    </div>
    <div class="row">
      @foreach ($block['pages'] as $key=>$item)
      <div class="col-lg-3 col-sm-6 {{$key < 1 ? "grid-margin" : ""}} mb-5 mb-sm-2">
        <div class="position-relative image-hover">
          <a style="text-decoration:none" href="{{route('page.show',$item->id)}}">
          <img
            src="{{asset('storage/'.$item->image)}}"
            class="img-fluid"
            alt="world-news"
          />
          <span class="thumb-title">{{$item->category_id}}</span>
          </a>
        </div>
        <h5 class="font-weight-bold mt-3">
          {{$item->title}}
        </h5>
        <p class="fs-15 font-weight-normal">
          {{$item->subtitle}}
        </p>
      </div>
      @endforeach
    </div>
  </div>
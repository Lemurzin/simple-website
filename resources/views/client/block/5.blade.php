  <!--editors-news-->
  <div class="editors-news">

    <div class="row">
      <div class="col-lg-3">
        <div class="d-flex position-relative float-left">
          <h3 class="section-title">{{$block['title']}}</h3>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6  mb-5 mb-sm-2">
        <div class="position-relative image-hover">
          <a style="text-decoration:none" href="{{route('page.show',$block['pages']['0']->id)}}">
            <img
              src="{{asset('storage/'.$block['pages']['0']->image)}}"
              class="img-fluid"
              alt="world-news"
            />
            <span class="thumb-title">{{$block['pages']['0']->category_id}}</span>
          </a>
        </div>
        <h1 class="font-weight-600 mt-3">
          {{$block['pages']['0']->title}}
        </h1>
        <p class="fs-15 font-weight-normal">
          {{$block['pages']['0']->subtitle}}
        </p>
      </div>

      <div class="col-lg-6  mb-5 mb-sm-2">
        <div class="row">
          <div class="col-sm-6  mb-5 mb-sm-2">
            <div class="position-relative image-hover">
              <a style="text-decoration:none" href="{{route('page.show',$block['pages']['1']->id)}}">
                <img
                  src="{{asset('storage/'.$block['pages']['1']->image)}}"
                  class="img-fluid"
                  alt="world-news"
                />
                <span class="thumb-title">{{$block['pages']['1']->category_id}}</span>
              </a>
            </div>
            <h5 class="font-weight-600 mt-3">
              {{$block['pages']['1']->title}}
            </h5>
            <p class="fs-15 font-weight-normal">
              {{$block['pages']['1']->subtitle}}
            </p>
          </div>
          <div class="col-sm-6  mb-5 mb-sm-2">
            <div class="position-relative image-hover">
              <a style="text-decoration:none" href="{{route('page.show',$block['pages']['2']->id)}}">
                <img
                  src="{{asset('storage/'.$block['pages']['2']->image)}}"
                  class="img-fluid"
                  alt="world-news"
                />
                <span class="thumb-title">{{$block['pages']['2']->category_id}}</span>
              </a>
            </div>
            <h5 class="font-weight-600 mt-3">
              {{$block['pages']['2']->title}}
            </h5>
            <p class="fs-15 font-weight-normal">
              {{$block['pages']['2']->subtitle}}
            </p>
          </div>
        </div>
        <div class="row mt-3">
          <div class="col-sm-6  mb-5 mb-sm-2">
            <div class="position-relative image-hover">
              <a style="text-decoration:none" href="{{route('page.show',$block['pages']['3']->id)}}">
              <img
                src="{{asset('storage/'.$block['pages']['3']->image)}}"
                class="img-fluid"
                alt="world-news"
              />
              <span class="thumb-title">{{$block['pages']['3']->category_id}}</span>
              </a>
            </div>
            <h5 class="font-weight-600 mt-3">
              {{$block['pages']['3']->title}}
            </h5>
            <p class="fs-15 font-weight-normal">
              {{$block['pages']['3']->subtitle}}
            </p>
          </div>
          <div class="col-sm-6">
            <div class="position-relative image-hover">
              <a style="text-decoration:none" href="{{route('page.show',$block['pages']['4']->id)}}">
              <img
                src="{{asset('storage/'.$block['pages']['4']->image)}}"
                class="img-fluid"
                alt="world-news"
              />
              <span class="thumb-title">{{$block['pages']['4']->category_id}}</span>
              </a>
            </div>
            <h5 class="font-weight-600 mt-3">
              {{$block['pages']['4']->title}}
            </h5>
            <p class="fs-15 font-weight-normal">
              {{$block['pages']['4']->subtitle}}
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
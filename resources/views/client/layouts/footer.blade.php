<footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="d-flex justify-content-between">
            <img src="assets/images/logo.svg" class="footer-logo" alt="" />

            <div class="d-flex justify-content-end footer-social">
              <h5 class="m-0 font-weight-600 mr-3 d-none d-lg-flex">Follow on</h5>
              @include('client.social-media.index')
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div
            class="d-lg-flex justify-content-between align-items-center border-top mt-5 footer-bottom"
          >
            <ul class="footer-horizontal-menu">
              <li><a href="#">xxx</a></li>
              <li><a href="#">xxx</a></li>
              <li><a href="#">xxx</a></li>
              <li><a href="#">xxx</a></li>
              <li><a href="#">xxx</a></li>
              <li><a href="#">xxx</a></li>
              <li><a href="#">xxx</a></li>
            </ul>
            <p class="font-weight-medium">
              © 2020 <a href="https://www.bootstrapdash.com/" target="_blank" class="text-dark">@ BootstrapDash</a>, Inc.All Rights Reserved.
            </p>
          </div>
        </div>
      </div>
    </div>
  </footer>
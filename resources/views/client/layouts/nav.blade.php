
<div
class="navbar-collapse justify-content-center collapse"
id="navbarSupportedContent"
>
<ul class="navbar-nav d-lg-flex justify-content-between align-items-center">
  <li class="nav-item">
      <a class="nav-link active" href="{{route('index')}}">Home</a>
  </li>
  @foreach ($categories as $category)
    <li class="nav-item active">
        <a class="nav-link active" href="{{route('category.index',$category)}}">{{$category->id}}</a>
    </li>
  @endforeach
</ul>
</div>
<!DOCTYPE html>
<html lang="zxx">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>World Vision</title>
    <link
      rel="stylesheet"
      href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}"
    />
    <link rel="stylesheet" href="{{asset('assets/vendors/aos/dist/aos.css/aos.css')}}" />
    <link
      rel="stylesheet"
      href="{{asset('assets/vendors/owl.carousel/dist/assets/owl.carousel.min.css')}}"
    />
    <link
      rel="stylesheet"
      href="{{asset('assets/vendors/owl.carousel/dist/assets/owl.theme.default.min.css')}}"
    />
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

  </head>

  <body>
    <div class="container-scroller">
      <div class="main-panel">
        <header id="header">
          <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
              <div class="d-flex justify-content-between align-items-center navbar-top">
                <ul class="navbar-left">
                </ul>
                <div>
                  <a class="navbar-brand" href="#"
                    ><img src="assets/images/logo.svg" alt=""
                  /></a>
                </div>
                <div class="d-flex">
                  @include('client.social-media.index')
                </div>
              </div>
              <div class="navbar-bottom-menu">
                <button
                  class="navbar-toggler"
                  type="button"
                  data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span class="navbar-toggler-icon"></span>
                </button>

                  @include('client.layouts.nav')
              </div>
            </nav>
          </div>
        </header>

        <div class="container">
          @yield('content')
        </div>

        @extends('client.layouts.footer')

      </div>
    </div>

    <script src="{{asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('assets/vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/demo.js')}}"></script>

  </body>
</html>

@extends('client.layouts.index')
@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="text-center">
      <h1 class="text-center mt-5">
        {{$category->id}}
      </h1>
      <p class="text-secondary fs-15">
        {{$category->title}}
      </p>
    </div>
  </div>
</div>
<div class="row">
@isset($pages[0])
  <div class="col-lg-6  mb-5 mb-sm-2">
    <div class="position-relative image-hover">
      <a style="text-decoration:none" href="{{route('page.show',$pages[0])}}">
        <img
          src="{{asset('storage/'.$pages[0]->image)}}"
          class="img-fluid"
          alt="world-news"
        />
        <!--<span class="thumb-title">{{$category->id}}</span>-->
      </a>
    </div>
    <h1 class="font-weight-600 mt-3">
      {{$pages[0]->title}}
    </h1>
    <p class="fs-15 font-weight-normal">
      {{$pages[0]->subtitle}}
    </p>
  </div>
@endisset

  <div class="col-lg-6  mb-5 mb-sm-2">
    <div class="row">
      @isset($pages[1])
      <div class="col-sm-6  mb-5 mb-sm-2">
        <div class="position-relative image-hover">
          <a style="text-decoration:none" href="{{route('page.show',$pages[1])}}">
            @isset($record)
                
            @endisset
            <img
              src="{{asset('storage/'.$pages[1]->image)}}"
              class="img-fluid"
              alt="world-news"
            />
            <!--<span class="thumb-title">{{$category->id}}</span>-->
          </a>
        </div>
        <h5 class="font-weight-600 mt-3">
          {{$pages[1]->title}}
        </h5>
        <p class="fs-15 font-weight-normal">
          {{$pages[1]->subtitle}}
        </p>
      </div>
      @endisset
      @isset($pages[2])
      <div class="col-sm-6  mb-5 mb-sm-2">
        <div class="position-relative image-hover">
          <a style="text-decoration:none" href="{{route('page.show',$pages[2])}}">
            <img
              src="{{asset('storage/'.$pages[2]->image)}}"
              class="img-fluid"
              alt="world-news"
            />
            <!--<span class="thumb-title">{{$category->id}}</span>-->
          </a>
        </div>
        <h5 class="font-weight-600 mt-3">
          {{$pages[2]->title}}
        </h5>
        <p class="fs-15 font-weight-normal">
          {{$pages[2]->subtitle}}
        </p>
      </div>
      @endisset
    </div>
    <div class="row mt-3">
      @isset($pages[3])
        <div class="col-sm-6  mb-5 mb-sm-2">
          <div class="position-relative image-hover">
            <a style="text-decoration:none" href="{{route('page.show',$pages[3])}}">
              <img
                src="{{asset('storage/'.$pages[3]->image)}}"
                class="img-fluid"
                alt="world-news"
              />
              <!--<span class="thumb-title">{{$category->id}}</span>-->
            </a>
          </div>
          <h5 class="font-weight-600 mt-3">
            {{$pages[3]->title}}
          </h5>
          <p class="fs-15 font-weight-normal">
            {{$pages[3]->subtitle}}
          </p>
        </div>
      @endisset
      @isset($pages[4])
        <div class="col-sm-6">
          <div class="position-relative image-hover">
            <a style="text-decoration:none" href="{{route('page.show',$pages[4])}}">
              <img
                src="{{asset('storage/'.$pages[4]->image)}}"
                class="img-fluid"
                alt="world-news"
              />
              <!--<span class="thumb-title">{{$category->id}}</span>-->
            </a>
          </div>
          <h5 class="font-weight-600 mt-3">
            {{$pages[4]->title}}
          </h5>
          <p class="fs-15 font-weight-normal">
            {{$pages[4]->subtitle}}
          </p>
        </div>
      @endisset
    </div>
  </div>
</div>
<!---->
<div class="row">
  @foreach ($pages as $key=>$page)
    @if ($key >= 5)
      <div class="col-sm-3  mb-5 mb-sm-2">
        <div class="position-relative image-hover">
          <a style="text-decoration:none" href="{{route('page.show',$page)}}">
            <img
              src="{{asset('storage/'.$page->image)}}"
              class="img-fluid"
              alt="world-news"
            />
            <!--<span class="thumb-title">{{$category->id}}</span>-->
          </a>
        </div>
        <h5 class="font-weight-600 mt-3">
          {{$page->subtitle}}
        </h5>
      </div>
    @endif
  @endforeach
  <div class="row">
    <div class="col">
      <div class="d-flex jastify-content-center">
        <div>{{$pages->links()}}</div>
      </div>
    </div>
  </div>
</div>
@endsection
   
@extends('client.layouts.index')
@section('content')

    <div class="contact-wrap">
      <div class="row">
        <div class="col-lg-12 mb-2 mb-lg-2">
          <div class="row">
            <div class="col-sm-12  mb-2 mb-lg-2">
              <h1>{{$page->title}}</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 mb-2 mb-lg-2">
          <div class="row">
            <div class="col-sm-12  mb-2 mb-lg-2">
              <p class="mb-4">
                {!! $page->content !!}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
   
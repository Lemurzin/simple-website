@extends('admin.layouts.index')
@section('title','Добавление страницы')
@section('content')
<a href="{{route('admin.page.index')}}" class="ml-4 btn btn-success mb-3">Назад</a>
<form action="{{route('admin.page.update',$page)}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('patch')
  <div class="card-body">
    <!--title-->

    <div class="form-group">
      <label>Заголовок<span class="text-danger">*<span></label>
      <input name="title" type="text" class="form-control" placeholder="Ввеите заголовок" value="{{$page->title}}">
      @error('title')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="form-group">
      <label>Подзаголовок</label>
      <input name="subtitle" type="text" class="form-control" placeholder="Ввеите заголовок" value="{{$page->subtitle}}">
      @error('subtitle')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <!--category_id-->
    <div class="row">
      <div class="col-12">
        <!-- select -->
        <div class="form-group">
          <label>Категория<span class="text-danger">*<span></label>
          <select name="category_id" class="form-control">
            @foreach ($categories as $category)
              <option value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
          </select>
          @error('category_id')
            <span class="text-danger">{{$message}}<span>
          @enderror
        </div>
      </div>
    </div>

    <!--img-->
    <div class="row">
      <div class="col-8">
        <div class="form-group">
          <label for="exampleInputFile">Втавить изображение<span class="text-danger">*<span></label>
          <div class="input-group">
            <div class="custom-file">
              <input name="image" type="file" class="custom-file-input">
              <label class="custom-file-label" for="exampleInputFile">Choose image</label>
            </div>
            <div class="input-group-append">
              <span class="input-group-text">Загрузить</span>
            </div>
          </div>
          @error('image')
            <span class="text-danger">{{$message}}<span>
          @enderror
        </div>
      </div>  
      <div class="col-4">
        <div style="height:100px">
          <img style="max-height:100%;overflow:auto;" src="{{asset('storage/'.$page->image)}}" alt="">
        </div>
      </div>
    </div>
    
    <!--active-->
    <div class="form-check">
      <input name="active" type="checkbox" class="form-check-input" value="1" {{$page->active == 1 ? "checked" : ""}}>
      <label>Видимость</label>
      @error('active')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <!--summernote-->
    <div class="form-group">
      <label>Основной контент<span class="text-danger">*<span></label>
      <textarea id="summernote" name="content">{{$page->content}}</textarea>
      @error('content')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <!--btn-->
    <div class="card-footer">
      <input type="submit" class="btn btn-primary" value="Изменить">
    </div>

  </div>
  <!-- /.card-body -->
</form>



@endsection

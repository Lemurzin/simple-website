@extends('admin.layouts.index')
@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">Страницы</h3>
            </div>
            <!-- ./card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="d-flex justify-content-between">
                            <a href="{{route('admin.page.create')}}" class="btn btn-success mb-3">Создать</a>

                            <form action="{{route('admin.page.index')}}">
                                <div class="input-group input-group-sm">
                                    <input name="title" type="text" class="form-control">
                                    <span class="input-group-append">
                                        <button type="submit" class="btn btn-info btn-flat">Поиск</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                    <th>id</th>
                    <th>Заголовок</th>
                    <th>Подзаголовок</th>
                    <th>Видимость</th>
                    <th>Категория</th>
                    <th>Дата публикации</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @isset($pages)
                        @foreach ($pages as $page)
                        <tr data-widget="expandable-table" aria-expanded="false">
                        <td>{{$page->id}}</td>
                        <td>{{$page->title}}</td>
                        <td>{{$page->subtitle}}</td>
                        <td>{{$page->active}}</td>
                        <td>{{$page->category_id}}</td>
                        <td class="d-flex justify-content-around">
                            <a href="{{route('admin.page.edit',$page->id)}}" class="btn btn-success">edit</a>
                            <form action="{{route('admin.page.destroy',$page)}}" method="POST">
                                @csrf
                                @method('delete')
                                <button onclick="return confirm('are you sure?')" class="btn btn-danger" type="submit">
                                    <i class="fas fa-trash" role="button"></i>
                                </button>
                            </form>
                        </td>
                        </tr>
                        <tr class="expandable-body d-none">
                        <td colspan="8">
                            <p style="display: none;">
                            {!! $page->content !!}
                            </p>
                        </td>
                        </tr>
                        @endforeach
                    @endisset
                </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
            
            <div>{{$pages->withQueryString()->links()}}</div>
        </div>
    </div>
@endsection

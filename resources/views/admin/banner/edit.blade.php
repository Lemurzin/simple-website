@extends('admin.layouts.index')
@section('title','Редактирование banner')
@section('content')
<a href="{{route('admin.banner.index')}}" class="ml-4 btn btn-success mb-3">Назад</a>
<form action="{{route('admin.banner.update',$banner)}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('patch')

  <div class="card-body">

    <div class="form-group">
      <label>Заголовок<span class="text-danger">*<span></label>
      <input name="title" type="text" class="form-control" placeholder="Введите title" value="{{$banner->title}}">
      @error('title')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="row">
      <div class="col-12">
        <div class="form-group">
          <label>Страница<span class="text-danger">*<span></label>
          <select name="page_id" class="form-control">
            @foreach ($pages as $page)
              <option value="{{$page->id}}">{{$page->title}}</option>
            @endforeach
          </select>
          @error('page_id')
            <span class="text-danger">{{$message}}<span>
          @enderror
        </div>
      </div>
    </div>

    <div class="card-footer">
      <input type="submit" class="btn btn-primary" value="Изменить">
    </div>

  </div>
</form>

@endsection

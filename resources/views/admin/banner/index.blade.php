@extends('admin.layouts.index')
@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">Banners</h3>
            </div>
            <div class="card-body">
                <a href="{{route('admin.banner.create')}}" class="btn btn-success mb-3">Создать</a>
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                    <th>id</th>
                    <th>Название</th>
                    <th>НомерСтраницы</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @isset($banners)
                        @foreach ($banners as $banner)
                        <tr data-widget="expandable-table" aria-expanded="false">
                        <td>{{$banner->id}}</td>

                        <td>{{$banner->title}}</td>
                        <td>{{$banner->page_id}}</td>

                        <td class="d-flex justify-content-center">
                            <a href="{{route('admin.banner.edit',$banner->id)}}" class="btn btn-success mr-1">edit</a>
                            
                            <div class="mr-1">
                                <form action="{{route('admin.banner.destroy',$banner)}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('are you sure?')" class="btn btn-danger" type="submit">
                                        <i class="fas fa-trash" role="button"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                        </tr>
                        @endforeach
                    @endisset
                </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
@endsection

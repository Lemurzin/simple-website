@extends('admin.layouts.index')
@section('title','Редактирование carousel')
@section('content')
<a href="{{route('admin.carousel.index')}}" class="ml-4 btn btn-success mb-3">Назад</a>
<form action="{{route('admin.carousel.update',$carousel)}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('patch')

  <div class="card-body">

    <div class="form-group">
      <label>Заголовок<span class="text-danger">*<span></label>
      <input name="title" type="text" class="form-control" placeholder="Введите title" value="{{$carousel->title}}">
      @error('title')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="form-group">
      <label>Подзаголовок<span class="text-danger">*<span></label>
      <input name="subtitle" type="text" class="form-control" placeholder="Введите subtitle" value="{{$carousel->subtitle}}">
      @error('subtitle')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="form-group">
      <label for="exampleInputFile">Втавить изображение<span class="text-danger">*<span></label>
      <div class="input-group">
        <div class="custom-file">
          <input name="image" type="file" class="custom-file-input">
          <label class="custom-file-label" for="exampleInputFile">Choose image</label>
        </div>
        <div class="input-group-append">
          <span class="input-group-text">Загрузить</span>
        </div>
      </div>
      @error('image')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="card-footer">
      <input type="submit" class="btn btn-primary" value="Изменить">
    </div>

  </div>
</form>

@endsection

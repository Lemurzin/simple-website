@extends('admin.layouts.index')
@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">Carousel</h3>
            </div>
            <div class="card-body">
                <a href="{{route('admin.carousel.create')}}" class="btn btn-success mb-3">Создать</a>
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Title</th>
                        <th>Subtitle</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @isset($carousel)
                        @foreach ($carousel as $el)
                        <tr data-widget="expandable-table" aria-expanded="false">
                            <td>{{$el->id}}</td>

                            <td>{{$el->title}}</td>
                            <td>{{$el->subtitle}}</td>

                            <td class="d-flex justify-content-center">
                                <a href="{{route('admin.carousel.edit',$el->id)}}" class="btn btn-success mr-1">edit</a>
                                
                                <div class="mr-1">
                                    <form action="{{route('admin.carousel.destroy',$el)}}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button onclick="return confirm('are you sure?')" class="btn btn-danger" type="submit">
                                            <i class="fas fa-trash" role="button"></i>
                                        </button>
                                    </form>
                                </div>
                                <a href="{{route('admin.carousel.show',$el)}}" class="btn btn-success">O</a>
                            </td>
                        </tr>
                        @endforeach
                    @endisset
                </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
@endsection

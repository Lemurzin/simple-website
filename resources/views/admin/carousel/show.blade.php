@extends('admin.layouts.index')
@section('title','Show carousel')
@section('content')
<a href="{{route('admin.carousel.index')}}" class="ml-4 btn btn-success mb-3">Назад</a>

<div class="card-body">
    <div class="row mb-3">
        <div class="col-sm-6">
        <img class="img-fluid" src="{{asset('storage/'.$carousel->image)}}" alt="Photo">
        </div>
        <div class="col-sm-6">
        <div class="row">
            <h4>{{$carousel->title}}</h4>
            <h6>{{$carousel->subtitle}}</h6>
        </div>
        </div>
    </div>
</div>

@endsection

@extends('admin.layouts.index')
@section('content')
    <div class="container-fluid">
        <div class="row">
            
            <div class="col-4">
                @include('admin.image.nav')
            </div>

            <div class="col-8">
                @yield('content-img')
            </div>
            
        </div>
    </div>
@endsection


@extends('admin.layouts.index')
@section('content')
<a href="{{route('admin.image.index')}}" class="ml-4 btn btn-success mb-3">Назад</a>
<form action="{{route('admin.image.store')}}" method="POST" enctype="multipart/form-data">
  @csrf
  
  <div class="card-body">
    <!--img-->
    <div class="form-group">

        <div class="input-group">
          <label>В папку</label>
          <input style="width:100%" name="directory" class="form-control text-dark" type="text" value="{{$path}}" readonly>
        </div>


        <div class="input-group">
          <label for="exampleInputFile">Втавить изображение</label>
        </div>
        <div class="input-group">
          <div class="custom-file">
            <input name="image" type="file" class="custom-file-input">
            <label class="custom-file-label" for="exampleInputFile">Choose image</label>
          </div>
          <div class="input-group-append">
            <span class="input-group-text">Загрузить</span>
          </div>
          @error('image')
            <span class="text-danger">{{$message}}<span>
          @enderror
        </div>
    </div>

    <!--btn-->
    <div class="card-footer">
      <input type="submit" class="btn btn-primary" value="Добавить">
    </div>
@endsection

<div>

    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">Добавить директорию</h3>
        </div>
        <form class="form-horizontal" action="{{route('admin.directory.store')}}" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input name="name" type="text" class="form-control" placeholder="Название">
                    </div>
                    @error('name')
                        <span class="text-danger">{{$message}}<span>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <input type="submit" class="btn btn-info" onclick="return confirm('Добавить новую директорию?')" value="Добавить">
            </div>
        </form>
    </div>

    
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">Список директорий</h3>
        </div>
        <div class="card-body table-responsive p-0" style="height: 300px;">
          <table class="table table-sm">
            <tbody>
                @foreach ($storages as $item)
                <tr>
                    <td><a href="{{route('admin.image.show',$item['name'])}}" class="nav-link">{{$item['name']}}</a></td>
                    <td class="pt-2">
                        <form action="{{route('admin.directory.destroy',$item['name'])}}" method="POST">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger btn-xs" type="submit" onclick="return confirm('Вы точно хотите удалить эту папку?')">
                                <i class="fas fa-trash" role="button"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
    </div>


</div>

                               
                       
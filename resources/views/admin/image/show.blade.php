@extends('admin.image.layout')
@section('content-img')     

<a href="{{route('admin.image.create',$name)}}" class="btn btn-success mb-3 col-12">Добавить изображение</a>

<div class="card card-info">
    <div class="card-header">
      <h3 class="card-title">Изображения</h3>
    </div>
    <div class="card-body table-responsive" style="height: 500px;">
      <div class="row">
        @foreach ($images as $img)
            <div class="col-sm-4 card-img">
                <div class="position-relative" style="min-height: 180px;">
                    <img src="{{asset('storage/'.$img['path'])}}" alt="{{$img['name']}}" class="img-fluid storage-img">
                </div>
            </div>
        @endforeach
      </div>
    </div>
  </div>
@endsection


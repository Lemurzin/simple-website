@extends('admin.layouts.index')
@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">Banners</h3>
            </div>
            <div class="card-body">
                <a href="{{route('admin.block.create')}}" class="btn btn-success mb-3">Создать</a>
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>title</th>
                        <th>position</th>
                        <th>isActive</th>
                        <th>page_ids</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @isset($blocks)
                        @foreach ($blocks as $block)
                        <tr data-widget="expandable-table" aria-expanded="false">
                        <td>{{$block->id}}</td>
                        <td>{{$block->title}}</td>
                        <td>{{$block->position}}</td>
                        <td>{{$block->isActive}}</td>
                        <td>{{$block->page_ids}}</td>

                        <td class="d-flex justify-content-center">
                            <a href="{{route('admin.block.edit',$block->id)}}" class="btn btn-success mr-1">edit</a>
                            
                            <div class="mr-1">
                                <form action="{{route('admin.block.destroy',$block)}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('are you sure?')" class="btn btn-danger" type="submit">
                                        <i class="fas fa-trash" role="button"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                        </tr>
                        @endforeach
                    @endisset
                </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
@endsection

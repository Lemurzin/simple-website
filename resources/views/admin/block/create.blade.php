@extends('admin.layouts.index')
@section('title','Добавление нового block')

@section('content')
<a href="{{route('admin.block.index')}}" class="ml-4 btn btn-success mb-3">Назад</a>
<form action="{{route('admin.block.store')}}" method="POST">
  @csrf
  <div class="card-body">

    <div class="form-group"> 
      <label>Заголовок<span class="text-danger">*<span></label>
      <input name="title" type="text" class="form-control" placeholder="Введите title" value="{{old('title')}}">
      @error('title')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="form-group"> 
      <label>Позиция<span class="text-danger">*<span></label>
      <input name="position" type="number" class="form-control" placeholder="Введите position" value="{{old('position')}}">
      @error('position')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="form-check">
      <input name="isActive" type="checkbox" class="form-check-input" value="1">
      <label>Видимость</label>
      @error('isActive')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="form-group">
      <label>Страницы<span class="text-danger">*<span></label>
      <div class="select2-purple">
        <select multiple name="pages[]" class="select2 form-control" data-dropdown-css-class="select2-purple">
          @foreach ($pages as $page)
            <option {{is_array(old('pages')) && in_array($page->id, old('pages')) ? 'selected' : ''}}
            value="{{$page->id}}">
              {{$page->title}}
            </option>
          @endforeach
        </select>
      </div>
      @error('pages[]')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="card-footer">
      <input type="submit" class="btn btn-primary" value="Добавить">
    </div>

  </div>
</form>

@endsection

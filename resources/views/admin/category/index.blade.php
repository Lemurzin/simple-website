@extends('admin.layouts.index')
@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">Категории</h3>
            </div>
            <!-- ./card-header -->
            <div class="card-body">
                <a href="{{route('admin.category.create')}}" class="btn btn-success mb-3">Создать</a>
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                    <th>id</th>
                    <th>Название</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @isset($categories)
                        @foreach ($categories as $category)
                        <tr data-widget="expandable-table" aria-expanded="false">
                        <td>{{$category->id}}</td>
                        <td>{{$category->title}}</td>
                        <td class="d-flex justify-content-center">
                            <a href="{{route('admin.category.edit',$category->id)}}" class="btn btn-success mr-1">edit</a>
                            <div class="mr-1">
                                <form action="{{route('admin.category.destroy',$category)}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('are you sure?')" class="btn btn-danger" type="submit">
                                        <i class="fas fa-trash" role="button"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                        </tr>
                        @endforeach
                    @endisset
                </tbody>
                </table>
                <div>{{$categories->links()}}</div>
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection

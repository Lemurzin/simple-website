@extends('admin.layouts.index')
@section('title','Редактирование категории')
@section('content')
<a href="{{route('admin.category.index')}}" class="ml-4 btn btn-success mb-3">Назад</a>
<form action="{{route('admin.category.update',$category)}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('patch')

  <div class="card-body">
    <!--title-->
    <div class="form-group">
      <label>ID<span class="text-danger">*<span></label>
      <input name="id" type="text" class="form-control" placeholder="Введите id" value="{{$category->id}}">
      @error('id')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="form-group">
      <label>Заголовок<span class="text-danger">*<span></label>
      <input name="title" type="text" class="form-control" placeholder="Ввеите title" value="{{$category->title}}">
      @error('title')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <!--btn-->
    <div class="card-footer">
      <input type="submit" class="btn btn-primary" value="Изменить">
    </div>

  </div>
  <!-- /.card-body -->
</form>



@endsection

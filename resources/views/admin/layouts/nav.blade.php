      
<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
      <li class="nav-header">EXAMPLES</li>
      <li class="nav-item">
        <a href="{{route('admin.category.index')}}" class="nav-link">
          <i class="nav-icon fas fa-th-list"></i>
          <p>
            Categories
            <span class="badge badge-info right">{{$counts['category']}}</span>
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.page.index')}}" class="nav-link">
          <i class="nav-icon far fa-file"></i>
          <p>
            Pages
            <span class="badge badge-info right">{{$counts['page']}}</span>
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.image.index')}}" class="nav-link">
          <i class="nav-icon far fa-image"></i>
          <p>
            Images
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-file"></i>
          <p>
            Index
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview" style="display: none;">
          <li class="nav-item">
            <a href="{{route('admin.banner.index')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Banners</p>
            </a>
          </li>
        </ul>
        <ul class="nav nav-treeview" style="display: none;">
          <li class="nav-item">
            <a href="{{route('admin.carousel.index')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Carousel</p>
            </a>
          </li>
        </ul>
        <ul class="nav nav-treeview" style="display: none;">
          <li class="nav-item">
            <a href="{{route('admin.block.index')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Blocks</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item">
        <a href="{{  route('admin.user.index')}}" class="nav-link">
          <i class="nav-icon fas fa-users"></i>
          <p>Users</p>
        </a>
      </li>
    
      <li style="margin-top:30%" class="nav-item">
        <form action="{{route('logout')}}" method="POST">
          @csrf
          <input class="ml-3 btn btn-outline-primary" type="submit" value="{{__('Exit')}}">
        </form>
      </li>
    </ul>
  </nav>
  <!-- /.sidebar-menu -->
@extends('admin.layouts.index')
@section('content')
<div class="row">
  <div class="col-12 col-sm-6 col-md-5">
    <div class="info-box">
      <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{$data[0]['title']}}</span>
        <span class="info-box-number">
          {{$data[0]['value']}}
        </span>
      </div>
    </div>

    <div class="info-box mb-3">
      <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{$data[1]['title']}}</span>
        <span class="info-box-number">{{$data[1]['value']}}</span>
      </div>
    </div>

  <div class="clearfix hidden-md-up"></div>

    <div class="info-box mb-3">
      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{$data[2]['title']}}</span>
        <span class="info-box-number">{{$data[2]['value']}}</span>
      </div>
    </div>

    <div class="info-box mb-3">
      <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{$data[3]['title']}}</span>
        <span class="info-box-number">{{$data[3]['value']}}</span>
      </div>
    </div>
  </div>

  <div class="col-md-7 col-sm-6">
    @if (isset($conf))
      <form action="{{route('admin.conf.update',$conf)}}" method="POST">
        @csrf
        @method('patch')
        <div class="card-body">
          <div class="form-check">
            <input name="hasLatest" type="checkbox" class="form-check-input" value="1" {{$conf->hasLatest == 1 ? "checked" : ""}}>
            <label>Отображать последние посты</label>
          </div>

          <div class="form-check">
            <input name="hasCarousel" type="checkbox" class="form-check-input" value="1" {{$conf->hasCarousel == 1 ? "checked" : ""}}>
            <label>Отображать карусель</label>
          </div>

          <div class="form-check">
            <input name="hasBanner" type="checkbox" class="form-check-input" value="1" {{$conf->hasBanner == 1 ? "checked" : ""}}>
            <label>Отображать баннер</label>
          </div>
        </div>
        
        <div class="card-footer">
          <input type="submit" class="btn btn-primary" value="Применить">
        </div>

      </form>
    @else
    <div class="alert alert-success" role="alert">
      Конфигурационный файл для отображения клиентской части пока не создан
    </div>
      <form action="{{route('admin.conf.store')}}" method="POST">
        @csrf
        <input type="hidden" name="hidden" value="exists">
        <div class="card-footer">
          <input type="submit" class="btn btn-primary" value="Создать">
        </div>
      </form>
    @endif
  </div>
</div>
@endsection
  
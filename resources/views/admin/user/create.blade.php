
@extends('admin.layouts.index')
@section('title','Добавление нового пользователя')

@section('content')
<a href="{{route('admin.user.index')}}" class="ml-4 btn btn-success mb-3">Назад</a>
<form action="{{route('admin.user.store')}}" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="card-body">

    <div class="form-group">
      <label>Имя<span class="text-danger">*<span></label>
      <input name="name" type="text" class="form-control" placeholder="Ввеите name" value="{{old('name')}}">
      @error('name')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="form-group">
      <label>Email<span class="text-danger">*<span></label>
      <input name="email" type="text" class="form-control" placeholder="Ввеите email" value="{{old('email')}}">
      @error('email')
        <span class="text-danger">{{$message}}<span>
      @enderror
    </div>

    <div class="form-group">
      <label>Password<span class="text-danger">*<span></label>
        <input name="password" type="password" class="form-control" placeholder="password" value="">
        @error('password')
          <span class="text-danger">{{$message}}<span>
        @enderror
    </div>

    <div class="form-group">
      <div class="mb-3">
        <select name="role" class="form-select form-select-m">
          <option value="null">Null</option>
          <option value="admin">admin</option>
        </select>
      </div>
    </div>

    <!--btn-->
    <div class="card-footer">
      <input type="submit" class="btn btn-primary" value="Добавить">
    </div>

  </div>
  <!-- /.card-body -->
</form>



@endsection

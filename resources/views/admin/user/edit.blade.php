@extends('admin.layouts.index')
@section('title','Редактирование администратора')
@section('content')
<a href="{{route('admin.user.index')}}" class="ml-4 btn btn-success mb-3">Назад</a>
<form action="{{route('admin.user.update',$user->id)}}" method="POST">
  @csrf
  @method('patch')
  <div class="mb-3">
    <input name="name" type="text" class="form-control" placeholder="name" value="{{$user->name}}">
    @error('name')
      <span class="text-danger">{{$message}}<span>
    @enderror
  </div>
  <div class="mb-3">
    <input name="email" type="email" class="form-control" placeholder="email" value="{{$user->email}}">
    @error('email')
      <span class="text-danger">{{$message}}<span>
    @enderror
  </div>
  <div class="mb-3">
    <input name="password" type="password" class="form-control" placeholder="password" value="">
    @error('password')
      <span class="text-danger">{{$message}}<span>
    @enderror
  </div>
  <div class="mb-3">
    <select name="role" class="form-select form-select-m">
      <option value="null">Null</option>
      <option value="admin">admin</option>
    </select>
  </div>
  <div class="card-footer">
    <input type="submit" class="btn btn-primary" value="Изменить">
  </div>
</form>

@endsection

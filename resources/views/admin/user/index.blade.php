@extends('admin.layouts.index')
@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">Категории</h3>
            </div>
            <!-- ./card-header -->
            <div class="card-body">
                <a href="{{route('admin.user.create')}}" class="btn btn-success mb-3">Создать</a>
                <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Email</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)
                    <tr>
                      <th scope="row">{{$user->id}}</th>
                      <td>{{$user->name}}</td>
                      <td>{{$user->role}}</td>
                      <td>{{$user->email}}</td>
                      <td>
                        <div style="display: inline-block">
                          <a href="{{route('admin.user.edit',$user->id)}}" class="btn btn-success">edit</a>
                        </div>
                        @if (auth()->user()->id !== $user->id)
                          <div style="display: inline-block">
                            <form action="{{route('admin.user.destroy', $user->id)}}" method="POST">
                              @csrf
                              @method('delete')
                              <input class="btn btn-danger" type="submit" value="delete"
                              onclick="return confirm('Are you sure?');">
                            </form>
                          </div>
                        @endif
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
